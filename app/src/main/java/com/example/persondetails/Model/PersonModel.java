package com.example.persondetails.Model;


public class PersonModel {
    private String login,
            id,
            avatar_url,
            url,
            location,
            email;

    public String getLocation() {
        return location;
    }
    public String getEmail() {
        return email;
    }
    public String getLogin() {
        return login;
    }
    public String getUrl() {
        return url;
    }

}
