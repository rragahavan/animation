package com.example.persondetails.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.persondetails.Model.PersonModel;
import com.example.persondetails.R;

import java.util.List;


public class UserNameAdapter extends RecyclerView.Adapter<UserNameAdapter.MyViewHolder> {
    final private List<PersonModel> nameList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        final public TextView nameText,
                urlText;
         public MyViewHolder(View view) {
            super(view);
            nameText = (TextView) view.findViewById(R.id.userName);
            urlText = (TextView) view.findViewById(R.id.userUrl);
            }
    }

    public UserNameAdapter(List<PersonModel> nameList) {
        this.nameList = nameList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_name_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserNameAdapter.MyViewHolder holder, int position) {
        PersonModel personModel =nameList.get(position);
        holder.nameText.setText(personModel.getLogin());
        holder.urlText.setText(personModel.getUrl());
    }

    @Override
    public int getItemCount() {
        return nameList.size();
    }
}
