package com.example.persondetails.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.persondetails.ApiServices.PersonDetails;
import com.example.persondetails.Model.PersonModel;
import com.example.persondetails.R;
import com.example.persondetails.Services.PersonService;

public class SingleUserDetailActivity extends AppCompatActivity {
    private ImageView profileImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_details);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
             TextView nameText,
                    emailText,
                    locationText,
                    titleText;
             String Item;

             ImageButton backArrow;
             PersonDetails personDetails;
             PersonService personService;
             PersonModel personModel;

            profileImage=(ImageView)findViewById(R.id.profileImage);
        personService=new PersonService(SingleUserDetailActivity.this);
        personModel=new PersonModel();
        Item = getIntent().getExtras().getString("Name");
        nameText=(TextView)findViewById(R.id.userName);
        emailText=(TextView)findViewById(R.id.userEmail);
        titleText=(TextView)findViewById(R.id.titleText);
        locationText=(TextView)findViewById(R.id.userLocation);
        backArrow=(ImageButton)findViewById(R.id.backArrowImage);
        if(!Item.equals(""))
            titleText.setText(Item);
        personDetails=new PersonDetails(this);
        personModel=personService.getSingleUser(Item);
        nameText.setText(personModel.getLogin());
        emailText.setText(personModel.getEmail());
        locationText.setText(personModel.getLocation());

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SingleUserDetailActivity.this,NameDisplayActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.activity_close_translate,R.anim.activity_close_translate);

            }
        });
        scaleAnimation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
        overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
    }
    public void scaleAnimation()
    {
        Animation mScaleAnim= AnimationUtils.loadAnimation(this,R.anim.scale_animation);
        profileImage.startAnimation(mScaleAnim);
    }
}
