package com.example.persondetails.ApiServices;

import android.content.Context;
import com.example.persondetails.R;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class PersonDetails {
    final private Context context;

    public PersonDetails(Context context){
        this.context = context;
    }

    public String readAllUserDetails()
    {
        URL url;
        StringBuilder sb =new StringBuilder();
        try {
            url = new URL(context.getResources().getString(R.string.read_all_user));
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            int status = urlConnection.getResponseCode();
            if(status==201||status==200){
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
            }
            urlConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public String singleUserDetails(String userName)
    {
        URL url;
        BufferedReader br;
        StringBuilder sb =new StringBuilder();
        try {
            url = new URL(context.getResources().getString(R.string.read_single_user)+userName);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            int status = urlConnection.getResponseCode();
            if(status==201||status==200){
                br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
            }
            urlConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
