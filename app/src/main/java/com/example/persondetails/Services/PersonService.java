package com.example.persondetails.Services;

import android.content.Context;
import android.widget.Toast;

import com.example.persondetails.ApiServices.PersonDetails;
import com.example.persondetails.Classes.ConnectionDetector;
import com.example.persondetails.Model.PersonModel;
import com.example.persondetails.R;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Arrays;

public class PersonService {
    final private Context context;
    final private PersonDetails personDetails;
    final private ConnectionDetector connectionDetector;
    public PersonService(Context context)
    {
        this.context=context;
        personDetails=new PersonDetails(context);
        connectionDetector=new ConnectionDetector(context);
    }

    public ArrayList<PersonModel> readUserDetails()
    {
        String personNames;
        Gson gson;
        ArrayList<PersonModel> personList=null;
        PersonModel[] personModels;

        if (connectionDetector.isNetworkAvailable()) {
            personNames = personDetails.readAllUserDetails();
            if (!personNames.equals("")) {
                gson = new Gson();
                personModels = gson.fromJson(personNames, PersonModel[].class);
                personList = new ArrayList<PersonModel>(Arrays.asList(personModels));
            }
            else
                Toast.makeText(context, R.string.server_unreachable, Toast.LENGTH_LONG).show();
        }
        else
            Toast.makeText(context, R.string.connection_problem, Toast.LENGTH_LONG).show();

        return personList;
    }

    public PersonModel getSingleUser(String userName)
    {
        String userDetails=personDetails.singleUserDetails(userName);
        PersonModel personModel;
        Gson gson = new Gson();
        personModel = gson.fromJson(userDetails, PersonModel.class);
        return personModel;
    }
}
